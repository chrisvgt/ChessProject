#include <iostream>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <vector>

// HEADER FILES
#include "../Field/Field.h"
#include "../GraphLib/Graph.h"

#ifndef CHESSBOARD_H
#define CHESSBOARD_H

class Chessboard
{
	class ChessGraph : public Graph // CAN ONLY EXIST WHEN CHESSBOARD EXISTS
	{
		friend Chessboard; // CHESSBOARD HAS ACCESS TO METHODS LIKE initializeGraph() WITHOUT BEING PUBLIC
	public:
		ChessGraph(Chessboard *chessboard);

	private:
		Chessboard *chessboard;
		void initializeGraph();
	};
	class FieldNode : public Node
	{
	public:
		FieldNode(std::string id) : Node(id) {}
		Field *field;
	};

public:
	Field fchessboard[8][8];
	ChessGraph graph;

	Chessboard();
	Chessboard(std::string filePath);
	~Chessboard();

	Field &findKnight();
	Field &findKing();
	void initialize(std::string filePath);
	std::stringstream draw();
};

#endif /* CHESSBOARD_H */