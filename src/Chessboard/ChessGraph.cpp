#include <string>
#include <map>

// HEADER FILES
#include "Chessboard.h"
#include "../Field/Color.h"

Chessboard::ChessGraph::ChessGraph(Chessboard *chessboard)
    : chessboard(chessboard) {}

void Chessboard::ChessGraph::initializeGraph() {
    std::map<std::string, FieldNode> createdNodes;

    // CREATE NODE, TYPE FIELD
    for (unsigned short i = 0; i < 8; i++) 
    {
        for (unsigned short j = 0; j < 8; j++) 
        {
            Field& field = chessboard->fchessboard[i][j];
            auto createdNode = create<FieldNode>(field.getField());
            createdNode.field = &field;
            createdNodes.insert(std::pair<std::string, FieldNode>(createdNode.getID(), createdNode)); // insert node in map

            // DEBUG
            std::clog << "DEBUG : node " << createdNode.getID() << " with color " << getColor(createdNode.field->getColor()) << " and ";
            if (createdNode.field->piece != nullptr) {
                std::clog << "a " << getColor(createdNode.field->piece->getColor()) << " " << createdNode.field->piece->getName();
            }
            else {
                std::clog << "no piece";
            }
            std::clog << "\n";
        }
    }

    // CREATE EDGES FROM CREATED NODES
    for (auto node : createdNodes)
    {
        short row;
        char col;
        std::string targetNode = "";

        // DEBUG
        if (node.second.field->piece != nullptr) {
            std::clog << "DEBUG : piece color " << node.second.getID() << ": " << getColor(node.second.field->piece->getColor()) << "\n";
        }

        if (!node.second.field->checkPieceColor(Color::WHITE) || chessboard->findKnight().getField() == node.second.field->getField()) 
        {
            if ((row = node.second.field->getRow() - 2) > 1 - 1) // 2 direction a
            { 
                if ((col = node.second.field->getColumn() - 1) > 'a' - 1) // 1 direction a 
                {
                    targetNode = Field::getField(col, row); 
                    std::clog << "DEBUG : edge from " << node.second.getID() << " to " << targetNode;
                    if (!createdNodes.at(targetNode).field->checkPieceColor(Color::WHITE)) { // not occupied by remoteNode figure, create edge
                        create<Edge>(*findNode(node.second.getID()), *findNode(targetNode));
                        std::clog << " ... success\n";
                    }
                    else {
                        std::clog << " was not created, because " << targetNode << " is occupied by a piece of the knight's team color\n";
                    }
                }

                if ((col = node.second.field->getColumn() + 1) < 'h' + 1) //1 direction h 
                {
                    targetNode = Field::getField(col, row);
                    std::clog << "DEBUG : edge from " << node.second.getID() << " to " << targetNode;
                    if (!createdNodes.at(targetNode).field->checkPieceColor(Color::WHITE))  // not occupied by remoteNode figure, create edge
                    {
                        create<Edge>(*findNode(node.second.getID()), *findNode(targetNode));
                        std::clog << " ... success\n";
                    }
                    else {
                        std::clog << " was not created, because " << targetNode << " is occupied by a piece of the knight's team color\n";
                    }
                }
            }

            if ((row = node.second.field->getRow() + 2) < 8 + 1) // 2 direction 8
            { 
                if ((col = node.second.field->getColumn() - 1) > 'a' - 1) // 1 direction a
                {
                    targetNode = Field::getField(col, row);
                    std::clog << "DEBUG : edge from " << node.second.getID() << " to " << targetNode;
                    if (!createdNodes.at(targetNode).field->checkPieceColor(Color::WHITE)) // not occupied by remoteNode figure, create edge
                    { 
                        create<Edge>(*findNode(node.second.getID()), *findNode(targetNode));
                        std::clog << " ... success\n";
                    }
                    else {
                        std::clog << " was not created, because " << targetNode << " is occupied by a piece of the knight's team color\n";
                    }
                }

                if ((col = node.second.field->getColumn() + 1) < 'h' + 1) { // 1 direction h
                    targetNode = Field::getField(col, row);
                    std::clog << "DEBUG : edge from " << node.second.getID() << " to " << targetNode;
                    if (!createdNodes.at(targetNode).field->checkPieceColor(Color::WHITE)) // not occupied by remoteNode figure, create edge
                    {
                        create<Edge>(*findNode(node.second.getID()), *findNode(targetNode));
                        std::clog << " ... success\n";
                    }
                    else {
                        std::clog << " was not created, because " << targetNode << " is occupied by a piece of the knight's team color\n";
                    }
                }
            }

            if ((row = node.second.field->getRow() - 1) > 1 - 1) // 1 direction 1
            {
                if ((col = node.second.field->getColumn() - 2) > 'a' - 1) // 2 direction a
                {
                    targetNode = Field::getField(col, row);
                    std::clog << "DEBUG : edge from " << node.second.getID() << " to " << targetNode;
                    if (!createdNodes.at(targetNode).field->checkPieceColor(Color::WHITE)) // not occupied by remoteNode figure, create edge
                    {
                        create<Edge>(*findNode(node.second.getID()), *findNode(targetNode));
                        std::clog << " ... success\n";
                    }
                    else {
                        std::clog << " was not created, because " << targetNode << " is occupied by a piece of the knight's team color\n";
                    }
                }

                if ((col = node.second.field->getColumn() + 2) < 'h' + 1) // 2 direction h
                {
                    targetNode = Field::getField(col, row);
                    std::clog << "DEBUG : edge from " << node.second.getID() << " to " << targetNode;
                    if (!createdNodes.at(targetNode).field->checkPieceColor(Color::WHITE)) // not occupied by remoteNode figure, create edge
                    {
                        create<Edge>(*findNode(node.second.getID()), *findNode(targetNode));
                        std::clog << " ... success\n";
                    }
                    else {
                        std::clog << " was not created, because " << targetNode << " is occupied by a piece of the knight's team color\n";
                    }
                }
            }

            if ((row = node.second.field->getRow() + 1) < 8 + 1) // 1 direction 8
            { 
                if ((col = node.second.field->getColumn() - 2) > 'a' - 1) // 2 direction a
                {
                    targetNode = Field::getField(col, row);
                    std::clog << "DEBUG : edge from " << node.second.getID() << " to " << targetNode;
                    if (!createdNodes.at(targetNode).field->checkPieceColor(Color::WHITE)) // not occupied by remoteNode figure, create edge
                    {
                        create<Edge>(*findNode(node.second.getID()), *findNode(targetNode));
                        std::clog << " ... success\n";
                    }
                    else {
                        std::clog << " was not created, because " << targetNode << " is occupied by a piece of the knight's team color\n";
                    }
                }

                if ((col = node.second.field->getColumn() + 2) < 'h' + 1) // 2 direction h
                { 
                    targetNode = Field::getField(col, row);
                    std::clog << "DEBUG : edge from " << node.second.getID() << " to " << targetNode;
                    if (!createdNodes.at(targetNode).field->checkPieceColor(Color::WHITE)) // not occupied by remoteNode figure, create edge
                    {
                        create<Edge>(*findNode(node.second.getID()), *findNode(targetNode));
                        std::clog << " ... success\n";
                    }
                    else {
                        std::clog << " was not created, because " << targetNode << " is occupied by a piece of the knight's team color\n";
                    }
                }
            }
        }
        else {
            std::clog << "DEBUG : no edges created from " << node.second.getID() << " because it's occupied by a piece of the knight's team!\n";
        }
    }
}