// HEADER FILES
#include "Chessboard.h"

Chessboard::Chessboard():graph(this){}

Chessboard::Chessboard(std::string filePath)
	:graph(this)
{
	initialize(filePath);
}

Chessboard::~Chessboard(){}

Field &Chessboard::findKnight() // SEARCH KNIGHT BY NAME
{
	Field* knight = new Field;
	for (int i = 0; i < 8; i++) 
	{
		for (int j = 0; j < 8; j++) 
		{
			if (fchessboard[i][j].piece != nullptr) 
			{
				if (fchessboard[i][j].piece->getName() == "Springer") 
				{
					delete knight;
					knight = &fchessboard[i][j];
				}
			}
		}
	}
	return *knight;
}

Field &Chessboard::findKing() // SEARCH KING BY NAME
{
	Field* king = new Field;
	for (int i = 0; i < 8; i++) 
	{
		for (int j = 0; j < 8; j++)
		{
			if (fchessboard[i][j].piece != nullptr) 
			{
				if (fchessboard[i][j].piece->getName() == "Koenig") 
				{
					delete king;
					king = &fchessboard[i][j];
				}
			}
		}
	}
	return *king;
}

void Chessboard::initialize(std::string filePath)
{
	// CREATE 8x8 CHESSBOARD IN 2 DIMENSIONAL ARRAY
	// x[0][0]   x[0][1]   ...
	// x[1][0]   x[1][1]   ...
	//   ...       ...     ...
	char columnLetter[] = { 'a','b','c','d','e','f','g','h'};

	for (int i = 0; i < 8; i++) 
	{
		for (int j = 0; j < 8; j++) 
		{
			fchessboard[i][j].setColumn(columnLetter[j]);
			fchessboard[i][j].setRow(8-i);

			if ((i % 2) != (j % 2)) // EVERY SECOND FIELD
			{
				fchessboard[i][j].setColor(Color::BLACK);
			}
			else fchessboard[i][j].setColor(Color::WHITE);
		}
	}

	// READ IN FILE
	std::clog << "READ IN FILE\n"; // DEBUG

	std::string STRING;
	std::fstream file;

	file.open(filePath);

	if (file.is_open() && file.good()) 
	{
		std::getline(file, STRING);
		file.close();
	}
	else 
	{
		std::clog << "\nFile ERROR!\n"; // DEBUG
		std::cout << "\nFile ERROR!\n";
		exit(2);
	}
	std::clog << "\nINPUT : " << STRING << "\n\n"; // DEBUG
	std::cout << "Ihre Eingabe lautet : " << STRING << "\n";

	// VALIDATE STRING
	std::clog << "CHECK ALLOWED CHARS\n";
	short king = 0;
	short knight = 0;
	bool validate = true;
	std::vector<char> allowedChars;

	for (int j = 49; j < 57; j++) allowedChars.push_back(char(j)); // 1-8
	for (int j = 97; j < 105; j++) allowedChars.push_back(char(j)); //a-h
	allowedChars.push_back('B');
	allowedChars.push_back('S');
	allowedChars.push_back('s');
	allowedChars.push_back('K');
	allowedChars.push_back('k');
	allowedChars.push_back(',');
	for (unsigned int i = 0; i < STRING.length(); i++) 
	{
		if (validate == false) 
		{
			std::clog << "ERROR : Invalid Input: "<< STRING[i-1] <<"\n"; // DEBUG
			std::cout << "Falsche Eingabe: "<< STRING[i-1] << "\n";
			exit(3);
		}
		validate = false;
		for (auto j = allowedChars.begin(); j != allowedChars.end(); ++j) 
		{
			if (*j == STRING[i]) // CHECK ALLOWEDCHARS
			{
				validate = !validate;
				switch (*j) // DENY DUPLICATES, ONLY ONE KING AND KNIGHT 
				{
				case 'K':
					knight++;
					break;
				case 'k':
					king++;
					break;
				case 'S':
					knight++;
					break;
				case 's':
					king++;
					break;
				default:
					break;
				}
				break;
			}
		}
	}
	if (knight > 1 || king > 1)
	{
		std::clog << "ERROR : King and Knight relation is wrong!\n"; // DEBUG
		std::cout << "\nSpringer kann Koenig nicht schlagen\n";
		exit(3);
	}
	else {
		std::cout << "Eingabe ist korrekt!\n";
		std::clog << "INPUT IS VALID!\n\n"; // DEBUG
	}

	// SLICE THE INPUT & CHECK FOR DUPLICATES
	std::vector<std::string> slicedStr;
	std::string cpStr = STRING;
	std::string tmpStr;
	std::size_t position;

	while (!cpStr.empty()) // UNTIL END
	{
		position = cpStr.find(',');
		tmpStr = cpStr.substr(0, position);
		slicedStr.push_back(cpStr.substr(0, position));
		cpStr.erase(0, 4);
		if (cpStr.find(tmpStr) != -1) 
		{
			std::clog << "ERROR : Duplicate Input" << "\n"; // ERROR
			std::cout << "Doppelte Figuren!\n" << "\n";
			exit(3);
		}
	}
	std::cout << "\nFiguren werden gesetzt ... \n";

	// CREATE PIECE OBJECTS
	std::clog << "DEBUG : CREATE PIECE OBJECTS\n"; // DEBUG

	for (short i = 0; i < 8; i++) 
	{
		for (short j = 0; j < 8; j++)
		{
			for (unsigned short k = 0; k < slicedStr.size(); k++)
			{
				if (fchessboard[i][j].getColumn() == slicedStr[k][1] && fchessboard[i][j].getRow() == slicedStr[k][2] - 48)
				{
					switch (slicedStr[k][0]) // SET COLOR BY CHAR
					{
					case 's':
						fchessboard[i][j].setPiece(Knight(), Color::WHITE);
						break;
					case 'S':
						fchessboard[i][j].setPiece(Knight(), Color::BLACK);
						break;
					case 'b':
						fchessboard[i][j].setPiece(Pawn(), Color::WHITE);
						break;
					case 'B':
						fchessboard[i][j].setPiece(Pawn(), Color::BLACK);
						break;
					case 'k':
						fchessboard[i][j].setPiece(King(), Color::WHITE);
						break;
					case 'K':
						fchessboard[i][j].setPiece(King(), Color::BLACK);
						break;
					default:
						std::clog << "ERROR : No object created.\n"; // ERROR
						exit(4);
					}
					if (fchessboard[i][j].piece != nullptr) 
					{
						// PRINT OUT THE ASSIGNMENTS
						std::cout << "-> " <<fchessboard[i][j].piece->getName() << " auf " << fchessboard[i][j].getField() << "\n";
						std::clog << "-> " <<fchessboard[i][j].piece->getName() << " auf " << fchessboard[i][j].getField() << "\n";
					}
				}
			}
		}
	}
	std::clog << "\nINITIAL THE GRAPH\n"; // DEBUG
	graph.initializeGraph();
}

std::stringstream Chessboard::draw()
{
	std::stringstream output;
		for (int i = 0; i < 8; i++)
		{
			if (i > 0) output << "\n"; // AXIS LABELING
			output << 8-i << " ";
			for (int j = 0; j < 8; j++)
			{
				output << fchessboard[i][j].draw().str();
			}
		}
		output << "\n   a  b  c  d  e  f  g  h\n\n";
	return output;
}

