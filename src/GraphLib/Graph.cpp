﻿#include "Graph.h"
#include <list>
#include <fstream>
#include <algorithm>
#include <limits>
#include <map>

//---------------------------------------------------------------------------------------------------------------------

Graph::~Graph()
{
	for (std::list<Edge *>::iterator it = m_edges.begin(); it != m_edges.end(); it++)
	{
		delete *it;
	}

	for (std::list<Node *>::iterator it = m_nodes.begin(); it != m_nodes.end(); it++)
	{
		delete *it;
	}
}

//---------------------------------------------------------------------------------------------------------------------

void Graph::remove(Node &rNode)
{
	// - alle Edges, die mit rNode verbunden sind, müssen entfernt werden!
	std::list<Edge *>::iterator it = m_edges.begin();
	while (it != m_edges.end())
	{
		Edge *pCurrEdge = *it;
		if (pCurrEdge->isConnectedTo(rNode))
		{
			it = m_edges.erase(it);
			delete pCurrEdge;
		}
		else
		{
			it++;
		}
	}

	// - der Pointer auf rNode soll aus m_nodes entfernt werden!
	m_nodes.remove(&rNode);
	// - der Pointer auf rNode muss mit 'delete' freigegeben werden!
	delete &rNode;
}

//---------------------------------------------------------------------------------------------------------------------

void Graph::remove(Edge &rEdge)
{
	std::list<Edge *>::iterator it = std::find(m_edges.begin(), m_edges.end(), &rEdge);
	if (it != m_edges.end())
	{
		delete *it;
		m_edges.erase(it);
	}
}

//---------------------------------------------------------------------------------------------------------------------

Node *Graph::findNode(const std::string &id)
{
	for (std::list<Node *>::iterator it = m_nodes.begin(); it != m_nodes.end(); it++)
	{
		Node *pCurrentNode = *it;
		if (pCurrentNode->getID() == id)
		{
			return *it;
		}
	}

	return NULL;
}

//---------------------------------------------------------------------------------------------------------------------

std::vector<Edge *> Graph::findEdges(const Node &rSrc, const Node &rDst)
{
	std::vector<Edge *> ret;

	for (std::list<Edge *>::iterator it = m_edges.begin(); it != m_edges.end(); it++)
	{
		// - findet alle edges, mit rSrc als Source-Node und rDst als Destination-Node.
		if ((&(*it)->getSrcNode()) == &rSrc && (&(*it)->getDstNode() == &rDst))
		{
			// - füge die Zeiger der Edges in den vector 'ret' ein.
			ret.push_back(*it);
		}
	}

	return ret;
}

//---------------------------------------------------------------------------------------------------------------------

void Graph::saveAsDot(const std::string &rFilename)
{
	/*
Kopieren Sie den Ordner ‚Peters\C23_Algorithmen_Programmierung\Tools\graphviz‘ im Dozentenserver auf ein lokales Laufwerk.

Graphiz ist ein Tool, welches Graphen aus einer textuellen Beschreibung erzeugen kann.
Betrachten Sie die Datei graph.dot.
Formal ist die Datei folgendermaßen aufgebaut :

digraph {
concentrate = true
<NODE_ID>;
<NODE_ID>;
<NODE_ID>;
<EDGE_1>;
<EDGE_2>;
<EDGE_N>;
}

Starten Sie die Datei make.bat, damit Graphiz ein Bild des Graphen erstellt.
Spielen Sie mit der Datei graph.dot herum und lassen Sie sich den Graph dazu generieren.


Implementieren Sie nun die Funktion 'Graph::saveAsDot', damit sie eine Dot-Datei im angegebenen Dateipfad erzeugt!

Hilfestellung:
- Dateien speichern mit C++: http://www.cplusplus.com/reference/fstream/fstream/open/
- Verwenden Sie die Funktionen Node::getID() und Edge::toString(), um die Einträge für die Nodes und Edges in der dot - Datei zu generieren.


TEST:
Testen Sie die Funktion, indem Sie indem Sie einen Graph in mit ein paar Nodes und Edges in main.cpp erstellen
und anschließend eine dot-Datei generieren. Erstellen Sie ein Bild des Graphen mit Graphviz.

*/
}

//---------------------------------------------------------------------------------------------------------------------

struct DijkstraNodeInfo
{
	double dist;
	Node *pPrevNode;
	Edge *pPrevEdge;
	bool bVisited;
};

void Graph::findShortestPathDijkstra(std::deque<Edge *> &rPath, const Node &rSrcNode, const Node &rDstNode)
{
	typedef std::map<Node *, DijkstraNodeInfo> tNodeMap;

	tNodeMap nodes;

	Node *pSrcNode = findNode(rSrcNode.getID());
	Node *pDstNode = findNode(rDstNode.getID());

	if (pSrcNode == NULL || pDstNode == NULL)
	{
		return;
	}

	for (std::list<Node *>::iterator it = m_nodes.begin(); it != m_nodes.end(); it++)
	{
		DijkstraNodeInfo info;
		info.dist = std::numeric_limits<double>::max();
		info.pPrevEdge = NULL;
		info.pPrevNode = NULL;
		info.bVisited = false;

		nodes[*it] = info;
	}

	nodes[pSrcNode].dist = 0;

	while (true)
	{
		Node *u = NULL;

		for (tNodeMap::iterator it = nodes.begin(); it != nodes.end(); it++)
		{
			Node *currNode = it->first;

			if ((it->second.bVisited == false) &&
				(u == NULL || it->second.dist < nodes[u].dist))
			{
				u = currNode;
			}
		}

		/* Abbruchkriterium: alle Nodes wurden besucht */
		if (u == NULL)
		{
			return;
		}

		nodes[u].bVisited = true;

		/* Abbruchkriterium: der gesuchte Node wurde gefunden */
		if (u == pDstNode)
		{
			while (u != NULL && nodes[u].pPrevEdge != NULL)
			{
				rPath.push_front(nodes[u].pPrevEdge);
				u = nodes[u].pPrevNode;
			}
			return;
		}

		for (std::list<Edge *>::iterator it = u->getOutEdges().begin();
			 it != u->getOutEdges().end(); it++)
		{
			Edge *pNeighbourEdge = *it;
			Node *v = &pNeighbourEdge->getDstNode();

			double newDistance = nodes[u].dist + pNeighbourEdge->getWeight();

			if (newDistance < nodes[v].dist)
			{
				nodes[v].dist = newDistance;
				nodes[v].pPrevNode = u;
				nodes[v].pPrevEdge = pNeighbourEdge;
			}
		}
	}
}

//---------------------------------------------------------------------------------------------------------------------
