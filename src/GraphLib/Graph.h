#include <list>
#include <vector>
#include <deque>
#include <string>
#include <algorithm>
#include "Node.h"
#include "Edge.h"

#ifndef GRAPH_H
#define GRAPH_H

class Graph
{

public:
    virtual ~Graph();

    // to create a Node
    template <class T>
    T &create(const std::string &id);

    // to create an Edge
    template <class T>
    T &create(Node &rSrcNode, Node &rDstNode);

    void remove(Node &rNode);

    void remove(Edge &rEdge);

    const std::list<Node *> &getNodes() { return m_nodes; }

    const std::list<Edge *> &getEdges() { return m_edges; }

    Node *findNode(const std::string &id);

    std::vector<Edge *> findEdges(const Node &rSrc, const Node &rDst);

    void saveAsDot(const std::string &rFilename);

    void findShortestPathDijkstra(std::deque<Edge *> &rPath, const Node &rSrcNode, const Node &rDstNode);

protected:
    std::list<Node *> m_nodes;
    std::list<Edge *> m_edges;
};

//---------------------------------------------------------------------------------------------------------------------

template <class T>
T &Graph::create(const std::string &id)
{
    Node *pNode = findNode(id);
    if (pNode == NULL)
    {
        pNode = new T(id);
        m_nodes.push_back(pNode);
    }

    return dynamic_cast<T &>(*pNode);
}

//---------------------------------------------------------------------------------------------------------------------

template <class T>
T &Graph::create(Node &rSrcNode, Node &rDstNode)
{
    //rSrcNode zum Graph hinzuf�gen, falls er noch nicht in m_nodes vorhanden ist.
    if (std::find(m_nodes.begin(), m_nodes.end(), &rSrcNode) == m_nodes.end())
    {
        m_nodes.push_back(&rSrcNode);
    }

    // rDstNode zum Graph hinzuf�gen, falls er noch nicht in m_nodes vorhanden ist.
    if (std::find(m_nodes.begin(), m_nodes.end(), &rDstNode) == m_nodes.end())
    {
        m_nodes.push_back(&rDstNode);
    }

    // - neue Edge erstellen (mit new)
    T *pEdge = new T(rSrcNode, rDstNode);
    // - die neue Edge in m_edges einf�gen
    m_edges.push_back(pEdge);
    // - Referenz auf die neue Edge zur�ck geben
    return *pEdge;
}

//---------------------------------------------------------------------------------------------------------------------

#endif
