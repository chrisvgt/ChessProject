#include <string>
#include <list>

#ifndef NODE_H
#define NODE_H
// forward-deklaration
class Edge;

class Node
{
public:
	Node();

	virtual ~Node() {}

	Node(std::string id) : m_id(id) { s_numInstances++; }

	std::string getID() const { return m_id; }

	std::list<Edge *> &getOutEdges() { return m_outgoingEdges; }
	std::list<Edge *> &getInEdges() { return m_incomingEdges; }

protected:
	std::list<Edge *> m_outgoingEdges;
	std::list<Edge *> m_incomingEdges;

private:
	std::string m_id;
	static int s_numInstances;
};

#endif