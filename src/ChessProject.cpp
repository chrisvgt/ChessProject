#define CREATE_DEBUG true

#include <iostream>
#include <sstream>
#include <thread>
#include <chrono>
#include <deque>

// HEADER FILES
#include "Chessboard/Chessboard.h"
#include "Field/Field.h"
#include "Field/Color.h"
#include "GraphLib/Edge.h"

int main()
{
    #if CREATE_DEBUG
        std::ofstream log("debug.log"); 
        std::clog.rdbuf(log.rdbuf()); 
    #else
        std::stringstream discard_log; 
        std::clog.rdbuf(discard_log.rdbuf());
    #endif
    std::clog << "\n### START LOGGING ###\n\n"; // DEBUG
    ///////////////////////////////////////////////////////////////////
    //     ____ _                   ____            _           _    //
    //    / ___| |__   ___  ___ ___|  _ \ _ __ ___ (_) ___  ___| |_  // 
    //   | |   | '_ \ / _ \/ __/ __| |_) | '__/ _ \| |/ _ \/ __| __| //
    //   | |___| | | |  __/\__ \__ \  __/| | | (_) | |  __/ (__| |_  //
    //    \____|_| |_|\___||___/___/_|   |_|  \___// |\___|\___|\__| //
    //                                           |__/                //
    //                        19.01.2020                             //
    ///////////////////////////////////////////////////////////////////

    // USER INTERFACE
    std::string inFile;
    std::cout << "########################\n";
    std::cout << "##### ChessProject #####\n";
    std::cout << "########################\n\n";
    std::cout << "Dieses Programm gibt den kuerzesten Weg eines Springer zum gegnerischen Koenig aus!\nMit Schlagen von gegnerischen Bauern und vermeiden friedlicher Bauern.\n\n";
    std::this_thread::sleep_for(std::chrono::seconds(2));
    std::cout << "Bitte geben Sie eine Text-Datei oder Beispiel an\n";
    std::cout << "Benutzen Sie bitte folgendes Format fuer Ihre eigene Textdatei.\n\n";
    std::cout << "[1 Stelle] Steht fuer den Typ der Figur und die Farbe \n";
    std::cout << "           { s -> weisser Springer | K -> schwarzer Koenig | b -> weisser Bauer}\n";
    std::cout << "[2 Stelle] waagerechte Position { a - h }\n";
    std::cout << "[3 Stelle] horizontale Position { 1 - 8 }\n\n";
    std::cout << "Beispiel einer 'example.txt'\n";
    std::cout << "sa1,bc2,bd4,be6,Kh8\n\n";
    std::cout << "Wenn Sie kein Board erstellen wollen, haben Sie eine Auswahl zwischen 1-4 Beispiel Schachbrettern.\n";
    std::cout << "Bei Eingabe einer Zahl von 1 bis 4, wird ein Beispiel geladen.\n\n";

    std::cout << "Zahl fuer Beispiel oder eigene Textdatei angeben: ";
    std::cin >> inFile;
    if (inFile == "1")
    {
        inFile = "Examples/Board1.txt";
    }
    else if (inFile == "2")
    {
        inFile = "Examples/Board2.txt";
    }
    else if (inFile == "3")
    {
        inFile = "Examples/Board3.txt";
    }
    else if (inFile == "4")
    {
        inFile = "Examples/Board4.txt";
    }

    // INITIALIZATION
    Chessboard ChessB(inFile);
    std::deque<Edge*> rpath;

    // CHESSBOARD VISUALIZATION
    std::cout << "\nDas Schachbrett wird erstellt!\n\n";
    std::this_thread::sleep_for(std::chrono::seconds(2));
    std::cout << ChessB.draw().str();

    // GET KING AND KNIGHT POSITIONS
    std::string knightNode = Field::getField(ChessB.findKnight().getColumn(), ChessB.findKnight().getRow());
    std::string kingNode = Field::getField(ChessB.findKing().getColumn() , ChessB.findKing().getRow());
    
    std::clog << "\nDEBUG : number of edges: " << ChessB.graph.getEdges().size() << "\n"; // DEBUG

    // FIND SHORTEST PATH VIA DIJLKSTRA ALGORITHM
    ChessB.graph.findShortestPathDijkstra(rpath, *ChessB.graph.findNode(knightNode), *ChessB.graph.findNode(kingNode));
    if (rpath.size() == 0)
    {
        std::cout << "Es gibt keinen Weg.\n";
        std::clog << "ERROR : No path possible. path.size() = " << rpath.size() << "\n"; // DEBUG
        return 1;
    }

    std::cout << "Dijkstra Algorithmus berechnet den Weg ";
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    for (int i = 0; i < 10; i++)
    {
        std::cout << ".";
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
    std::clog << "\n\n"; // DEBUG
    std::cout << "\n\n";

    // OUTPUT
    std::clog << "OUTPUT: " << rpath.at(0)->getSrcNode().getID(); // DEBUG
    std::cout << "Ausgabe: " << rpath.at(0)->getSrcNode().getID();
    for (auto i : rpath)
    {
        std::clog << " -> "; // DEBUG
        std::cout << " -> ";
        std::clog << i->getDstNode().getID(); // DEBUG
        std::cout << i->getDstNode().getID();
    }
    std::clog << "\n\nSUCCESS!\n";
    std::cout << "\n\nDas Programm wurde erfolgreich ausgefuehrt!\n";

    return 0;
}