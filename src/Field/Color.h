#include <string>

#ifndef COLOR_H
#define COLOR_H

enum class Color
{
    BLACK,
    WHITE
};
std::string getColor(Color color);

#endif /* COLOR_H */