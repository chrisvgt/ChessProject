#pragma once
#include <sstream>
#include <memory>

// HEADER FILES
#include "Color.h"
#include "Piece/Piece.h"
#include "Piece/King.h"
#include "Piece/Knight.h"
#include "Piece/Pawn.h"

#ifndef FIELD_H
#define FIELD_H

class Field
{
private:
	unsigned short itsRow; //1,2,3,4,5,6,7,8
	char itsColumn;		   //a,b,c,d,e,f,g,h
	Color itsColor;

public:
	Field();

	std::stringstream draw();

	void setRow(short int rowNumber);
	void setColumn(char columnLetter);
	void setColor(Color color);

	char getColumn() const;
	unsigned short getRow() const;
	Color getColor();

	std::unique_ptr<Piece> piece;
	bool checkPieceColor(Color color);
	bool checkNullptr();

	void setPiece(King type, Color color);
	void setPiece(Pawn type, Color color);
	void setPiece(Knight type, Color color);

	std::string getField() const;
	static std::string getField(char column, short row);
};

#endif /* FIELD_H */
