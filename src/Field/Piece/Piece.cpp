#include "Piece.h"

void Piece::setColor(Color color)
{
	itsColor = color;
}

Color Piece::getColor()
{
	return itsColor;
}
