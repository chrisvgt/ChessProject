#include "Piece.h"

#ifndef PAWN_H
#define PAWN_H

class Pawn: public Piece
{
private:
	std::string itsName = "Bauer";
public:
	std::string getName();
	char getCharName();
};

#endif /* PAWN_H */
