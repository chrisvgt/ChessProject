#include "Piece.h"

#ifndef KING_H
#define KING_H
class King : public Piece
{
private:
	std::string itsName = "Koenig";

public:
	std::string getName();
	char getCharName();
};

#endif
