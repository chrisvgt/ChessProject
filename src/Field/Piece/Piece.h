#include <iostream>
#include <memory>

#include "../Color.h"

#ifndef PIECE_H
#define PIECE_H
class Piece
{
public:
	void setColor(Color color);
	Color getColor();

	virtual std::string getName() = 0;
	virtual char getCharName() = 0;

private:
	std::string name;
	Color itsColor;
};

#endif /* PIECE_H */
