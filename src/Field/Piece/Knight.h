#include "Piece.h"

#ifndef KNIGHT_H
#define KNIGHT_H

class Knight : public Piece
{
private:
	std::string itsName = "Springer";

public:
	std::string getName();
	char getCharName();
};

#endif /* KNIGHT */
