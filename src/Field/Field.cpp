// HEADER FILES
#include "Field.h"

Field::Field()
	: itsRow(1), itsColumn('a'), itsColor(Color::WHITE)
{
}

std::stringstream Field::draw()
{
	std::stringstream output;

	if (getColor() == Color::BLACK)
	{
		output << " ";
		if (checkNullptr() == true)
		{
			if (piece->getColor() == Color::WHITE)
			{
				output << "\033[32m" << piece->getCharName() << "\033[0m"; // team green
			}
			else
				output << "\033[31m" << piece->getCharName() << "\033[0m"; // team red
		}
		else
			output << " ";
		output << " ";
	}
	else if (getColor() == Color::WHITE)
	{
		output << "\033[47m \033[0m";
		if (checkNullptr() == true)
		{
			if (piece->getColor() == Color::WHITE)
			{
				output << "\033[47m\033[32m" << piece->getCharName() << "\033[0m"; // team green
			}
			else
				output << "\033[47m\033[31m" << piece->getCharName() << "\033[0m"; // team red
		}
		else
			output << "\033[47m \033[0m";
		output << "\033[47m \033[0m";
	}
	else
	{
		std::cout << "color assignment error!\n";
		exit(4);
	}
	return output;
}

void Field::setRow(short int rowNumber)
{
	itsRow = rowNumber;
}

void Field::setColumn(char columnLetter)
{
	itsColumn = columnLetter;
}

void Field::setColor(Color color)
{
	itsColor = color;
}

char Field::getColumn() const
{
	return itsColumn;
}

unsigned short Field::getRow() const
{
	return itsRow;
}

Color Field::getColor()
{
	return itsColor;
}

bool Field::checkPieceColor(Color color)
{
	if (color == Color::WHITE)
	{
		if (checkNullptr())
		{
			if (piece->getColor() == Color::WHITE)
			{
				return true;
			}
		}
	}
	else if (color == Color::BLACK)
	{
		if (checkNullptr())
		{
			if (piece->getColor() == Color::BLACK)
			{
				return true;
			}
		}
	}
	return false;
}

bool Field::checkNullptr()
{
	if (piece != nullptr)
	{
		return true;
	}
	return false;
}

void Field::setPiece(King type, Color color)
{
	piece = std::unique_ptr<Piece>(new King);
	piece->setColor(color);
}

void Field::setPiece(Pawn type, Color color)
{
	piece = std::unique_ptr<Piece>(new Pawn);
	piece->setColor(color);
}

void Field::setPiece(Knight type, Color color)
{
	piece = std::unique_ptr<Piece>(new Knight);
	piece->setColor(color);
}

std::string Field::getField() const
{
	return getField(getColumn(), getRow());
}

std::string Field::getField(char column, short row)
{
	return column + std::to_string(row);
}