#include "Color.h"

std::string getColor(Color color)
{
    if (color == Color::WHITE)
    {
        return "WHITE";
    }
    else
    {
        return "BLACK";
    }
}
