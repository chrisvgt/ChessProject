# SpringerProject
University project assignment from the module Algorithms, Data Structures & Complexities.  

The goal of the module was to solve the "Springer problem" using Dijkstra's algorithm.

<img src="chessproject.png" alt="User Interface" width="700">
